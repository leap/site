#!/bin/bash

echo "you should change deploy.sh when you want to publish rendered sites"
echo -e "\033[0;32mDeploying updates to GitHub...\033[0m"

#if public exists remove it
if [ -d "public" ]
  then rm -rf public
fi

mkdir public

#cd public

#git init
#git checkout -b no-masters
#git remote add -m no-masters origin https://0xacab.org/leap/site.git
#git branch -u origin/no-masters
#
#git pull origin no-masters

#cd ..

# Build the project.
hugo env production || hugo --debug # if using a theme, replace with `hugo -t <YOURTHEME>`

# Go To Public folder
cd public
# Add changes to git.
git add .

# Commit changes.
#msg="rebuilding site `date`"
#if [ $# -eq 1 ]
#  then msg="$1"
#fi
#git commit -m "$msg"
#
## Push source and build repos.
#git remote add origin git@0xacab.org:leap/site.git
#git push --set-upstream origin main
#
# Come Back up to the Project Root
cd ..
