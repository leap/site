# LEAP Encryption Access Project

## Changes? Imrpovements? More content? Better memes?

Create a pull request or an issue at https://0xacab.org/leap/site

## Local test

``` ➜  leap.se.24 git:(main) docker run --rm -it \
  -v $(pwd):/src \
  -p 1313:1313 \
  floryn90/hugo:ext-asciidoctor server
```
Watch and checkout at http://localhost:1313/

## Deploy

Push it to 0xacab, watch https://leap.se



## This is what the developer thought to be a good way and has more details, so check it out:

## Prerequisites

Before starting, ensure you have Hugo installed on your system for building and previewing the website. Node.js and npm are required only for full development, including style adjustments.

## Quick Start with Hugo

If you only need to update the website content and do not need to modify styles or scripts:

1. **Clone the Repository** (replace `your-repository-url` with the actual URL of the repository):

   ```bash
   git clone your-repository-url
   cd leap-website
   ```
2. **Run Hugo Server**

   To preview your content changes in real time, run:

   ```bash
   hugo server
   ```

   This will start the Hugo server, making the site available on `http://localhost:1313/`. Changes made to the content files will be automatically reloaded in the browser.

## Full Development Setup

For full development, including CSS with Tailwind, you will need to install Node.js and npm.

1. **Install Node.js and npm**

   Download and install Node.js from [nodejs.org](https://nodejs.org/). npm is included with Node.js installation.
2. **Install Dependencies**

   Once Node.js and npm are installed, run the following command in the project directory to install necessary packages:

   ```bash
   npm install
   ```
3. **Run Development Environment**

   To work with Tailwind CSS and have live reloading for both Hugo content and Tailwind styles, you can use an npm script defined in `package.json`. Typically, this script runs both Hugo and a Tailwind JIT compiler:

   ```bash
   npm run dev
   ```

   This script will watch for changes in both your content and CSS, compiling everything in real time for the best development experience.

## Project Structure

- **assets/**: Contains CSS, icons, and images used throughout the website.
- **config/**: Configuration files for Hugo, including settings for different languages and menus.
- **content/**: Markdown files for the website's content, organized by language.
- **layouts/**: Custom HTML templates for rendering the website.
- **static/**: Static files like fonts and icons used across the site.
- **netlify.toml**: Configuration for deploying the site with Netlify.
- **package.json**: Lists npm dependencies and scripts for building the site.

## Deployment

Deployment configurations are specified in `netlify.toml`, which includes commands for building the site and specifying the directory to publish. This file is used by Netlify to automate the deployment process from pushing changes to the repository to hosting the live site, but you can deploy on any of the similar services, like Cloudflare Pages or AWS Amplify, and many others.
