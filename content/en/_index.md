---
layout: blocks
title: LEAP Encryption Access Project
description: LEAP Encryption Access Project

blocks:
  - block: hero
    image: /images/leap.svg
    heading: |-
      Privacy Should  
      Not Be A Privilege
    copy: |-
      LEAP is dedicated to giving internet users access to secure communication. We are a development shop of FLOSS advocates and practitioners. We come from international solidarity networks and are active participants in the internet freedom community. Our primary product is LEAP VPN, an open source white label VPN designed for ease of use and utility within censored environments. LEAP VPN is the shared code base for RiseupVPN, CalyxVPN, and Bitmask.

  - block: features
    background: muted
    heading: LEAP VPN
    copy: |-
      LEAP VPN is designed for mid scale deployments with a focus on supporting journalists, activists, dissidents, and human rights defenders, and their affinity networks. All our apps are 100% open source. Our entire platform orchestration system for VPN deployment is 100% open source and well documented to ease provisioning. LEAP VPN code has been third party audited by Cure53 and is fast, reliable, and easy to use. We specialize in field testing and providing censorship bypassing technology.
    image: /images/leap-vpn.png
    table:
      title: "Features"

      heading:
        - title: "Windows"
        - title: "macOS"
        - title: "Linux"
        - title: "Android"

      rows:
        - title: "Code is open source"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "UDP and TCP"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "Snowflake"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "Obfs4"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "VPN Hotspot: android only"
          checkmark:
            -
            -
            -
            - true

  - block: list
    heading: LEAP VPN Providers
    copy: |-
      We work with trusted service providers to build and brand their VPN service.  Our trusted partners, Riseup and The Calyx Institute have a long public history of protecting their users and adhere to best practices for VPN providers.
    items:
      - image: /images/icons/riseup.svg
        copy: |-
          **RiseupVPN**: A free service for censorship circumvention, location anonymization and traffic encryption. Unlike most free providers, Riseup does not log your IP address, or mine your data.
        buttons:
          - text: Get the app
            url: https://riseup.net/vpn
      - image: /images/icons/calyx.svg
        copy: |-
          **CalyxVPN**: By encrypting and securely routing all your network traffic through Calyx’s servers, CalyxVPN is able to prevent many forms of censorship and surveillance.
        buttons:
          - text: Get the app
            url: https://calyxos.org/docs/guide/apps/calyx-vpn/
      - image: /images/icons/bitmask.svg
        copy: |-
          **Bitmask**: This isn’t a provider, but it is LEAP’s very own VPN client that allows you to select from multiple service providers all from one app. Current providers include Riseup and The Calyx Institute.
        buttons:
          - text: Google Play
            url: https://play.google.com/store/apps/details?id=se.leap.bitmaskclient&hl=en_US&gl=US
          - text: F-Droid
            url: https://f-droid.org/en/packages/se.leap.bitmaskclient/

  - block: recent-posts
    background: muted
    heading: Recent Posts

  - block: text
    background:
    id: get-involved
    heading: Get Involved
    copy: |-
      **We value your contribution!**\
      LEAP thrives when there are contributions from the community. Many of our current team started by submitting patches and fixing bugs that were bothering them.

      **Code & bugs and fixes**\
      You can find developer documentation here: https://docs.leap.se/\
      You can find code and project plans on this [Gitlab](https://0xacab.org/leap) instance. You can also find us on [Github](https://github.com/leapcode).

      **Translations and localization**\
      Transifex is where we're gathering translations by volunteers like you. The Localization Lab helps us and they've got a [short intro on how to contribute](https://wiki.localizationlab.org/index.php/Bitmask).

      **Talk to us**\
      The best way to get in touch is via matrix: [#leap:systemli.org](https://matrix.to/#/#leap:systemli.org)
---
