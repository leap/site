---
# Display name
title: onyinyang

# author (this should match the folder name)
author: onyinyang

# Role/position
role: Censorship Circumvention Researcher

# Short bio (displayed in user profile at end of posts)
bio: Recent graduate (MMath) of the CrySP lab at the University of Waterloo. Now researching UDP based pluggable transports with traffic obfuscation at LEAP.

social:
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/algorithknit
  - icon: link
    icon_pack: fas
    link: https://onyiny-ang.github.io/
---
