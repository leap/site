---
title: "Donate"
description: "Donate"
layout: blocks

blocks:
  - block: hero
    image: /images/leap.svg
    heading: |-
      Privacy Should  
      Not Be A Privilege

  - block: text
    heading: Donate
    copy: |-
      LEAP depends on donations and grants to keep doing the work we do. Please donate today if you value secure communication that is easy for both the end-user and the service provider.

      LEAP is a non-profit incorporated in Washington State, USA. However, you contributions are not tax deductable.

      By Mail
      -------

      > LEAP Encryption Access Project
      >
      > PO Box 3027
      >
      > Lacey, WA 98509 USA

      Bitcoin
      -------

      > bc1q8k02k0fy2q69ukxnm6d5zelh4cm8ryfd72uurc

      Paypal
      ------

  - block: donation
---
