---
title: "Wanted: Android Developer to Fight Censorship"
slug: "android-dev-wanted-sequel"
summary: "The LEAP Encryption Access Project (LEAP) is looking for a mobile application developer to join our team! Work will include development of LEAP VPN, a multi-platform VPN application with a focus on usability and censorship-circumvention, Tor VPN, and Pluggable Transports integration. The candidate must be self motivated, self managed and able to work well within a highly collaborative team. The candidate must be experienced with Android and Golang development. A passion for internet freedom and privacy is a big plus."
authors:
- leap
tags:
- job
categories:
- vpn
- dev
date: "2024-06-21T00:00:00Z"
featured: true
draft: false
---

The LEAP Encryption Access Project (LEAP) is looking for a mobile application developer to join our team! Work will include development of LEAP VPN, a multi-platform VPN application with a focus on usability and censorship-circumvention, Tor VPN, and Pluggable Transports integration. The candidate must be self motivated, self managed and able to work well within a highly collaborative team. The candidate must be experienced with Android and Golang development. A passion for internet freedom and privacy is a big plus. (https://leap.se)

* **Time:** Part time
* **Where**: Remote
* **Start Date:** ASAP
* **Pay:** $40-50/hr

### **Job description**

If you join the team, you will be part of the development of LEAP VPN, a white-label free software VPN application based on OpenVPN that is currently deployed by our grassroots privacy focused partner providers, Riseup and Calyx. You will also work with LEAP developers and Tor developers on a new onion routing app that uses Android's VPN API underneath.

You should have an in-depth understanding of the Android SDK and ideally have experience with VPN API Android offers. You should be familiar with writing testable code, unit tests and instrumentation tests to guarantee code quality. You should be able to write Java and Kotlin code to meet the reality of maintaining a long-standing codebase.

You have experience with Golang? Great! We’re sharing code between our Desktop and Android client and develop and integrate circumvention tech in our LEAP VPN clients using Golang.

Your work on the new onion routing app makes having Rust experience a plus.

This is a remote position: our team is fully distributed with staff in India, Germany, Spain, and USA.

Do you already have a job with more than 20 hours per week? Our experience has taught us that this vacancy requires attention and commitment, so we will probably not be able to work together in such circumstances.

We particularly welcome applications from developers who are underrepresented in the tech industry.

### **Required qualifications**

* You must be a self-directed person, able to work efficiently under light management, and work well within a distributed team.
* Good communication skills in English language are essential.
* Profound knowledge of Android development with Java and Kotlin
* Profound knowledge of Golang
* Experience with Docker
* Good testing and debugging skills
* Familiarity with git and GitLab
* Experience with Open Source software development
* Be comfortable working remotely with a geographically distributed team

### **Preferred qualifications**

* Knowledge of Rust
* Familiarity with OpenVPN or Wireguard
* Familiarity with censorship circumvention technologies, like VPNs, Tor and Pluggable Transports, such as Snowflake or obfs4 implementations
* Experience with Ansible
* Ability to quickly prototype and deploy network services, perform measurements and iterate as part of a collaborative design process

### **LEAP’s Crew**

We’ve been building tools for secure communication for over a decade. Our team is kind, supportive,and passionate about the work we do. Our streamlined agile development process is light on management and heavy on collaboration. Our team is fully distributed, with multiple in person gatherings each year. We look forward to meeting you!

### **How to Apply**

To apply, please email us the following documents to info (at) leap (dot) se.

- Do not send a PDF: use any plaintext format at your convenience (md, latex, .c, .go, etc).
- Cover letter. Let us know how your qualificaitons and experience meet the requirements of this job position. Please include the reasons why you want to work with us. Include a link to your public GitHub/GitLab profile
- CV/resume
- Make sure to include at least 3 code samples
