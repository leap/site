---
title: Blog
heading: Recent Posts
layout: blocks

blocks:
  - block: hero
    image: /images/leap.svg
    heading: |-
      Privacy Should  
      Not Be A Privilege

  - block: blog
    heading: Blog Posts
---
