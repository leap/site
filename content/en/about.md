---
title: "About us"
description: "About us"
layout: blocks

blocks:
  - block: hero
    image: /images/leap.svg
    heading: |-
      Privacy Should  
      Not Be A Privilege

  - block: text
    heading: About Us
    copy: |-
      Founded in 2012, LEAP is dedicated to giving all internet users access to secure communication. There are many fronts in the fight against the enclosure of the internet and the relentless efforts to surveil and monetize our lives. We are a small and growing effort to expand the use of circumvention technology to protect us from government and corporate incursion into our online lives.

      We build a best in class, easy to use, VPN (Virtual Private Network), a widely used technology that encrypts your connection to the internet. Using a VPN requires people to place their trust in both the software and service provider that they choose. LEAP's code will always be Free Open Source Software, open to be inspected by anyone. Additionally, we only partner with trusted service providers who have a record of protecting their users' privacy.

  - block: text
    background: muted
    heading: Our Crew
    copy: |-
      **Our Partner Providers**: We partner with Riseup and The Calyx Institute to build and brand their VPN service. Riseup is a longtime provider of online communication tools for people and groups working on liberatory social change. Their email and mailing lists are used by hundreds of thousands of user around the world. RiseupVPN is a free to use donation supported VPN built on LEAP VPN. The Calyx Institute has a long track record of educating users about the threat of online surveillance and providing tools to protect their privacy, and is headed by Nicholas Merrill, one of the first people to successfully fight a National Security Letter.

      **Our Staff**: Our distributed team hails from all around the world, and currently includes people from: India, Germany, Spain, USA and Russia. Our team works directly with OONI, Tor and others in the internet freedom community.

      **Our Board**: LEAP is a non-profit organization registered in the state of Washington, USA. Our board of directors is comprised of open source advocates with deep roots in circumvention technologies and the internet freedom space.)

  - block: logos
    heading: Thank you for supporting LEAP
    copy: |-
      LEAP is funded via direct donations to LEAP and from users of our partner VPN services. We have also been sustained through the years with generous grants from the funders below.
    logos:
      - name: Open Technology Fund
        image: /images/logos/otf.png
      - name: Mozilla
        image: /images/logos/mozilla.png
      - name: nlnet
        image: /images/logos/nlnet.png
      - name: Internews
        image: /images/logos/internews.png
      - name: Prototype Fund
        image: /images/logos/prototype-fund.png
      - name: Hivos
        image: /images/logos/hivos.png
      - name: Digital Defenders Partnership
        image: /images/logos/digital-defenders-partnership.png
      - name: IMLS
        image: /images/logos/imls.png
      - name: Google Summer of Code
        image: /images/logos/google-summer-of-code.png
      - name: Wallace Global Fund
        image: /images/logos/wallace-global-fund.png
      - name: Tides
        image: /images/logos/tides.png
      - name: Institute For International Education
        image: /images/logos/institute-for-international-education.png
      - name: accessnow
        image: /images/logos/accessnow.png
      - name: The Calyx Institute
        image: /images/logos/calyx.png
      - name: Riseup Labs
        image: /images/logos/riseup.png

  - block: text
    background: muted
    heading: Get Involved
    copy: |-
      **We value your contribution!**\
      LEAP thrives when there are contributions from the community. Many of our current team started by submitting patches and fixing bugs that were bothering them.

      **Code & bugs and fixes**\
      You can find developer documentation here: https://docs.leap.se/\
      You can find code and project plans on this [Gitlab](https://0xacab.org/leap) instance. You can also find us on [Github](https://github.com/leapcode).

      **Translations and localization**\
      Transifex is where we're gathering translations by volunteers like you. The Localization Lab helps us and they've got a [short intro on how to contribute](https://wiki.localizationlab.org/index.php/Bitmask).

      **Talk to us**\
      The best way to get in touch is via matrix: [#leap:systemli.org](https://matrix.to/#/#leap:systemli.org)
---
