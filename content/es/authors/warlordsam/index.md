---
# Display name
title: warlordsam

# author (this should match the folder name)
author: warlordsam

# Role/position
role: GSoC'22 Contributor

# Short bio (displayed in user profile at end of posts)
bio: Hey, My name is Pratik Lagaskar (alias I use - warlordsam). I am a Second Year Undergraduate student, currently studying Electronics and Telecommunications Engineering with interest in cybersecurity and open-source.

social:
  - icon: twitter
    icon_pack: fab
    link: https://twitter.com/warlordsam077
  - icon: github
    icon_pack: fab
    link: https://github.com/WarlordSam07
  - icon: linkedin
    icon_pack: fab
    link: https://www.linkedin.com/in/pratik-lagaskar-a8747b20a/
---
