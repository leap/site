---
# Display name
title: LEAP

# author (this should match the folder name)
author: leap

# Role/position
role: LEAP Encryption Access Project

# Short bio (displayed in user profile at end of posts)
bio: fighting for the right to whisper

social:
  - icon: envelope
    link: "mailto:discuss-subscribe@leap.se"
  - icon: twitter
    link: https://twitter.com/leapcode
  - icon: github
    link: https://github.com/leapcode
---

We believe all internet users should have access to secure communications. Encryption technologies need to be easy to use and widely available.

We try to solve hard problems, connecting technologists, researchers and grassroots organizations. As part of this work, we provide tools to build your own VPN infrastructure, and client applications to connect to it. Easy. Secure. Beautiful.
