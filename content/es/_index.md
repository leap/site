---
layout: blocks
title: LEAP Encryption Access Project
description: Proyecto de Acceso a la Encriptación LEAP

blocks:
  - block: hero
    image: /images/leap.svg
    heading: |-
      La privacidad no debería ser un privilegio
    copy: |-
      LEAP se dedica a brindar a los usuarios de Internet acceso a la comunicación segura. Somos un grupo de desarrolladores defensores y practicantes del software libre y de código abierto (FLOSS). Provenimos de redes internacionales de solidaridad y somos participantes activos en la comunidad de libertad en Internet. Nuestro producto principal es LEAP VPN, una VPN de marca blanca de código abierto diseñada para ser fácil de usar y útil en entornos censurados.

  - block: features
    background: muted
    heading: VPN LEAP
    copy: |-
      VPN LEAP está diseñada para implementaciones de mediana escala con un enfoque en apoyar a periodistas, activistas, disidentes y defensores de los derechos humanos, y sus redes de afinidad. Todas nuestras aplicaciones son 100% de código abierto. Todo nuestro sistema de orquestación de plataforma para implementación de VPN es 100% de código abierto y está bien documentado para facilitar el aprovisionamiento. El código de VPN LEAP ha sido auditado por terceros por Cure53 y es rápido, confiable y fácil de usar. Nos especializamos en pruebas de campo y en proporcionar tecnología de bypass de censura.
    image: /images/leap-vpn.png
    table:
      title: "Características"

      heading:
        - title: "Windows"
        - title: "macOS"
        - title: "Linux"
        - title: "Android"

      rows:
        - title: "El código es de código abierto"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "UDP y TCP"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "Snowflake"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "Obfs4"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "Hotspot VPN: solo Android"
          checkmark:
            -
            -
            -
            - true

  - block: list
    heading: Proveedores de VPN LEAP
    copy: |-
      Trabajamos con proveedores de servicios confiables para construir y diseñar su servicio de VPN. VPN LEAP es la base de código compartida para RiseupVPN, CalyxVPN y Bitmask. Nuestros socios de confianza, Riseup y The Calyx Institute, tienen una larga historia pública de proteger a sus usuarios y cumplir con las mejores prácticas para los proveedores de VPN.
    items:
      - image: /images/icons/riseup.svg
        copy: |-
          **RiseupVPN**: Un servicio gratuito para la circunvención de la censura, la anonimización de la ubicación y el cifrado del tráfico. A diferencia de la mayoría de los proveedores gratuitos, Riseup no registra su dirección IP ni extrae sus datos.
        buttons:
          - text: Obtener la aplicación
            url: https://riseup.net/vpn
      - image: /images/icons/calyx.svg
        copy: |-
          **CalyxVPN**: Al cifrar y enrutar de forma segura todo su tráfico de red a través de los servidores de Calyx, CalyxVPN puede prevenir muchas formas de censura y vigilancia.
        buttons:
          - text: Obtener la aplicación
            url: https://calyxos.org/docs/guide/apps/calyx-vpn/
      - image: /images/icons/bitmask.svg
        copy: |-
          **Bitmask**: Esto no es un proveedor, pero es el propio cliente VPN de LEAP que le permite seleccionar entre varios proveedores de servicios, todo desde una sola aplicación. Los proveedores actuales incluyen Riseup y The Calyx Institute.
        buttons:
          - text: Google Play
            url: https://play.google.com/store/apps/details?id=se.leap.bitmaskclient&hl=en_US&gl=US
          - text: F-Droid
            url: https://f-droid.org/en/packages/se.leap.bitmaskclient/

  - block: recent-posts
    background: muted
    heading: Publicaciones recientes

  - block: text
    background:
    id: get-involved
    heading: Involúcrate
    copy: |-
      **¡Valoramos tu contribución!**\
      LEAP prospera cuando hay contribuciones de la comunidad. Muchos de nuestro equipo actual comenzaron enviando parches y corrigiendo errores que les molestaban.

      **Código, errores y correcciones**\
      Puedes encontrar documentación para desarrolladores aquí: https://docs.leap.se/\
      Puedes encontrar código y planes de proyecto en esta [instancia de Gitlab](https://0xacab.org/leap). También puedes encontrarnos en [Github](https://github.com/leapcode).

      **Traducciones y localización**\
      Transifex es donde estamos reuniendo traducciones de voluntarios como tú. El Localization Lab nos ayuda y tienen una [breve introducción sobre cómo contribuir](https://wiki.localizationlab.org/index.php/Bitmask).

      **Háblanos**\
      La mejor manera de ponerse en contacto es a través de Matrix: [#leap:systemli.org](https://matrix.to/#/#leap:systemli.org)
---
