---
title: "Sobre nosotros"
description: "Sobre nosotros"
layout: blocks

blocks:
  - block: hero
    image: /images/leap.svg
    heading: |-
      La privacidad no debería ser un privilegio

  - block: text
    heading: Sobre Nosotros
    copy: |-
      Fundado en 2012, LEAP se dedica a brindar a todos los usuarios de Internet acceso a comunicación segura. Hay muchos frentes en la lucha contra el cierre de Internet y los incansables esfuerzos por vigilar y monetizar nuestras vidas. Somos un esfuerzo pequeño y en crecimiento para expandir el uso de la tecnología de circunvención para protegernos de la incursión gubernamental y corporativa en nuestras vidas en línea.

      Construimos una VPN (Red Privada Virtual) fácil de usar y de primera clase, una tecnología ampliamente utilizada que cifra su conexión a Internet. Usar una VPN requiere que las personas confíen tanto en el software como en el proveedor de servicios que elijan. El código de LEAP siempre será software libre y de código abierto, abierto para ser inspeccionado por cualquier persona. Además, solo nos asociamos con proveedores de servicios confiables que tienen un historial de protección de la privacidad de sus usuarios.

  - block: text
    background: muted
    heading: Nuestro Equipo
    copy: |-
      **Nuestros Proveedores Asociados**: Nos asociamos con Riseup y The Calyx Institute para construir y diseñar su servicio de VPN. Riseup es un proveedor de herramientas de comunicación en línea de larga data para personas y grupos que trabajan en cambios sociales liberadores. Su correo electrónico y sus listas de correo son utilizados por cientos de miles de usuarios en todo el mundo. RiseupVPN es un servicio de VPN gratuito para usar, respaldado por donaciones, construido sobre VPN LEAP. The Calyx Institute tiene un largo historial de educar a los usuarios sobre la amenaza de la vigilancia en línea y proporcionar herramientas para proteger su privacidad, y está encabezado por Nicholas Merrill, una de las primeras personas en luchar con éxito contra una Carta de Seguridad Nacional.

      **Nuestro Equipo**: Nuestro equipo distribuido proviene de todo el mundo e incluye actualmente personas de: India, Alemania, España, Estados Unidos y Rusia. Nuestro equipo trabaja directamente con OONI, Tor y otros en la comunidad de la libertad en internet.

      **Nuestra Junta**: LEAP es una organización sin fines de lucro registrada en el estado de Washington, EE.UU. Nuestra junta directiva está compuesta por defensores del código abierto con profundos lazos en tecnologías de elusión y el espacio de libertad en internet.

  - block: logos
    heading: Gracias por apoyar a LEAP
    copy: |-
      LEAP es financiado mediante donaciones directas a LEAP y de usuarios de nuestros servicios de VPN asociados. También hemos sido sostenidos a lo largo de los años con generosas subvenciones de los financiadores a continuación.
    logos:
      - name: Open Technology Fund
        image: /images/logos/otf.png
      - name: Mozilla
        image: /images/logos/mozilla.png
      - name: nlnet
        image: /images/logos/nlnet.png
      - name: Internews
        image: /images/logos/internews.png
      - name: Prototype Fund
        image: /images/logos/prototype-fund.png
      - name: Hivos
        image: /images/logos/hivos.png
      - name: Digital Defenders Partnership
        image: /images/logos/digital-defenders-partnership.png
      - name: IMLS
        image: /images/logos/imls.png
      - name: Google Summer of Code
        image: /images/logos/google-summer-of-code.png
      - name: Wallace Global Fund
        image: /images/logos/wallace-global-fund.png
      - name: Tides
        image: /images/logos/tides.png
      - name: Institute For International Education
        image: /images/logos/institute-for-international-education.png
      - name: accessnow
        image: /images/logos/accessnow.png
      - name: The Calyx Institute
        image: /images/logos/calyx.png
      - name: Riseup Labs
        image: /images/logos/riseup.png

  - block: text
    background: muted
    heading: Involúcrate
    copy: |-
      **¡Valoramos tu contribución!**\
      LEAP prospera cuando hay contribuciones de la comunidad. Muchos de nuestro equipo actual comenzaron enviando parches y corrigiendo errores que les molestaban.

      **Código, errores y correcciones**\
      Puedes encontrar documentación para desarrolladores aquí: https://docs.leap.se/\
      Puedes encontrar código y planes de proyecto en esta [instancia de Gitlab](https://0xacab.org/leap). También puedes encontrarnos en [Github](https://github.com/leapcode).

      **Traducciones y localización**\
      Transifex es donde estamos reuniendo traducciones de voluntarios como tú. El Localization Lab nos ayuda y tienen una [breve introducción sobre cómo contribuir](https://wiki.localizationlab.org/index.php/Bitmask).

      **Háblanos**\
      La mejor manera de ponerse en contacto es a través de Matrix: [#leap:systemli.org](https://matrix.to/#/#leap:systemli.org)
---
