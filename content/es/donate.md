---
title: "Involúcrate"
description: "Involúcrate"
layout: blocks

blocks:
  - block: hero
    image: /images/leap.svg
    heading: |-
      La privacidad no debería ser un privilegio

  - block: text
    heading: Donar
    copy: |-
      LEAP depende de donaciones y subvenciones para seguir haciendo el trabajo que hacemos. Por favor, done hoy si valora la comunicación segura que es fácil tanto para el usuario final como para el proveedor de servicios.

      LEAP es una organización sin fines de lucro incorporada en el estado de Washington, EE. UUU.

      Por correo

      ***

      > LEAP Encryption Access Project
      >
      > PO Box 3027
      >
      > Lacey, WA 98509 USA

      Bitcoin

      ***

      > bc1q8k02k0fy2q69ukxnm6d5zelh4cm8ryfd72uurc

      Paypal
      ------

  - block: donation
---
