---
layout: blocks
title: LEAP Encryption Access Project
description: Проект доступа к шифрованию LEAP

blocks:
  - block: hero
    image: /images/leap.svg
    heading: |-
      Приватность не должна быть привилегией
    copy: |-
      LEAP посвящен предоставлению пользователям интернета доступа к безопасной коммуникации. Мы - команда разработчиков, пропагандистов и практиков свободного программного обеспечения. Мы приходим из международных солидарностных сетей и активно участвуем в сообществе интернет-свободы. Наш основной продукт - LEAP VPN, открытый исходный код виртуальной частной сети, разработанный для простоты использования и полезности в цензурируемых средах.

  - block: features
    background: muted
    heading: LEAP VPN
    copy: |-
      LEAP VPN разработан для масштабных развертываний с акцентом на поддержку журналистов, активистов, диссидентов, защитников прав человека и их сетей аффилированных лиц. Все наши приложения на 100% открытые исходные коды. Наша вся система оркестровки платформы для развертывания VPN на 100% открытая и хорошо документирована для упрощения предоставления. Код LEAP VPN прошел проверку от третьей стороны Cure53 и является быстрым, надежным и простым в использовании. Мы специализируемся на полевых испытаниях и предоставлении технологии обхода цензуры.

    image: /images/leap-vpn.png
    table:
      title: "Особенности"

      heading:
        - title: "Windows"
        - title: "macOS"
        - title: "Linux"
        - title: "Android"

      rows:
        - title: "Исходный код открыт"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "UDP и TCP"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "Снежинка"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "Obfs4"
          checkmark:
            - true
            - true
            - true
            - true
        - title: "VPN-точка доступа: только для Android"
          checkmark:
            -
            -
            -
            - true

  - block: list
    heading: Поставщики VPN LEAP
    copy: |-
      Мы сотрудничаем с надежными поставщиками услуг, чтобы создавать и брендировать их сервис VPN. VPN LEAP является общим исходным кодом для RiseupVPN, CalyxVPN и Bitmask. Наши доверенные партнеры, Riseup и The Calyx Institute, имеют долгую публичную историю защиты своих пользователей и придерживаются лучших практик для поставщиков VPN.
    items:
      - image: /images/icons/riseup.svg
        copy: |-
          **RiseupVPN**: Бесплатный сервис для обхода цензуры, анонимизации местоположения и шифрования трафика. В отличие от большинства бесплатных провайдеров, Riseup не ведет журнал вашего IP-адреса и не добывает ваши данные.
        buttons:
          - text: Получить приложение
            url: https://riseup.net/vpn
      - image: /images/icons/calyx.svg
        copy: |-
          **CalyxVPN**: Шифруя и безопасно направляя весь ваш сетевой трафик через серверы Calyx, CalyxVPN способен предотвратить многие формы цензуры и слежения.
        buttons:
          - text: Получить приложение
            url: https://calyxos.org/docs/guide/apps/calyx-vpn/
      - image: /images/icons/bitmask.svg
        copy: |-
          **Bitmask**: Это не поставщик, но это собственный клиент VPN LEAP, который позволяет выбирать из нескольких поставщиков услуг, все из одного приложения. Существующие поставщики включают Riseup и The Calyx Institute.
        buttons:
          - text: Google Play
            url: https://play.google.com/store/apps/details?id=se.leap.bitmaskclient&hl=en_US&gl=US
          - text: F-Droid
            url: https://f-droid.org/en/packages/se.leap.bitmaskclient/

  - block: recent-posts
    background: muted
    heading: Недавние сообщения

  - block: text
    background:
    id: get-involved
    heading: Присоединяйтесь
    copy: |-
      **Мы ценим ваш вклад!**\
      LEAP процветает, когда есть вклад от сообщества. Многие из нашей текущей команды начали с отправки патчей и исправления ошибок, которые их беспокоили.

      **Код и исправления ошибок**\
      Здесь вы можете найти документацию разработчика: https://docs.leap.se/\
      Вы также можете найти код и планы проектов на этом [Gitlab](https://0xacab.org/leap). Также вы можете найти нас на [Github](https://github.com/leapcode).

      **Переводы и локализация**\
      Transifex - это место, где мы собираем переводы от добровольцев, таких как вы. Локализационная лаборатория помогает нам, и у них есть [краткое введение в то, как внести свой вклад](https://wiki.localizationlab.org/index.php/Bitmask).

      **Общение с нами**\
      Лучший способ связаться с нами - через Matrix: [#leap:systemli.org](https://matrix.to/#/#leap:systemli.org)
---
