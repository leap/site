---
title: "A resilient core"
slug: "a-resilient-core"
summary: "We live in bad times for tunneling traffic. In the last year, we've seen backlash against VPNs and other circumvention technology in several countries. In the past few months, we've been working on streamlining the possibility to deploy a diversity of circumvention tactics based on Pluggable Transports."
authors:
  - leap
tags:
  - pluggable-transport
  - OpenVPN
  - platform
categories:
  - vpn
  - dev
date: "2023-08-12T00:00:00Z"
lastmod: "2023-08-12T00:00:00Z"
featured: true
draft: false
---

We live in bad times for tunneling traffic. In the last year, we've seen
backlash against VPNs and other circumvention technology in several countries.

In the past few months, we've been working on streamlining the possibility to deploy
a diversity of circumvention tactics based on Pluggable Transports.

## menshen: the protector of gates

menshen is our gateway coordinator. [We have revamped the
service](https://0xacab.org/leap/menshen/-/tree/lb-integration?ref_type=heads)
to make a few decisions more easily configurable - which will impact the default degree of
visibility of regular gateways and obfuscated bridges. menshen now generates an OpenAPI spec,
which allow us to seamlessly update supporting libraries in several languages.

We also took the opportunity to improve and merge a sidecar service that
informs a central coordinator of the load at each gateway and bridge, to avoid
overloading a specific node in the network.

While doing this, we have also added support for introducers, a new obfuscation
concept that we will explain in more detail in future posts.

In terms of existing providers, care is being taken so that there exists a
smooth migration path between v3 and v5 versions of the API - for the time
being, the new v5 menhen API can be bootstrapped from a legacy v3
configuration.

## A new core

There's a new Go library that makes use of the auto-generated client code, and streamlines all the bootstrapping operations under a single API. New obfuscation capabilities have been incorporated in [bitmask-core](https://0xacab.org/leap/bitmask-core), like uTLS support for parroting, DNS over TLS resolution, arbitrary SOCKS5 support (so that the user can decide to use Tor to boostrap certificates). Support for the new introducers has been already baked-in.

There's an experimental desktop support for openvpn3-linux (the new dbus interface). Our roadmap includes migrating Android, Desktop and the coming iOS client to this new library.

## Better geographical allocation

Traditionally, menshen had a geolocation service that helped to allocate gateways by spherical proximity. We've spent some time simulating allocation based on an empirical latency matrix, and came to the conclusion that live congestion monitoring is more important, for the size of the providers we're working with, than having a perfect geolocation. For the time being, we're considering decoupling the geolocation service (which means one less database to maintain). In practice this means the provider can decide whether to run (and maintain) this microservice or not.

Additionally, bitmask-core can now sort a range of locations based on latency measurements. We've also added an embedded database that will keep track of successful bridge connections, so that the clients can have some kind of memory built-in to "remember" the preferred locations.

## a jnk is a fast type of boat

Float is the foundation of the LEAP Platform - basically, ansible on steroids.
For the sake of quick deployments - mostly for development and quick experiments, we've published a new tool called [jnk](https://0xacab.org/atanarjuat/jnk). It's still unstable and highly experimental, but if you want to get your hands dirty with the development of openvpn or obfsvpn bridges, it might come handy (it basically orchestrates the various containers in a single-node fashion, like what you would do with docker compose, but saving a few logical swtiches here and there).

Part of the migration of the services also involve modernizing a bit the
OpenVPN setup - we've been experimenting with faster ECDH keys for OpenVPN, and
including kernel-based acceleration in the OpenVPN containers.

## Acknowledgements

Thanks to Internews for offering us the opportunity to work in circumvention tech and giving some love to the maintenance of code for critical infrastructure.
