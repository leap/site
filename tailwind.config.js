module.exports = {
  content: ["./layouts/**/*.html", "./content/**/*.md"],
  safelist: ["bg-white"],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        brand: "#DD378A",
        muted: "#f7f7f7",
      },
      fontSize: {
        asize: ["19px", "30px"],
        bsize: ["35px", "44px"],
      },
      typography: {
        DEFAULT: {
          css: {
            color: "#333",
            textDecoration: "none",
            blockquote: {
              color: "#000",
              background: "#f7f7f7",
              fontStyle: "normal",
              fontWeight: "700",
              padding: "1.25rem",
              borderLeftWidth: "4px",
              borderLeftColor: "#DD378A",
              paddingTop: "2px",
              paddingBottom: "2px",
            },
            a: {
              textDecoration: "underline",
              "&:hover": {
                opacity: "0.8",
              },
            },
            strong: {
              fontWeight: "700",
            },
          },
        },
      },
      fontFamily: {
        montserrat: ["Montserrat", "sans-serif"],
        sans: ["Open Sans", "sans-serif"],
      },
    },
  },
  plugins: [require("@tailwindcss/typography"), require("@tailwindcss/forms")],
};
